# GitLab Runner

Der GitLab Runner ist eine Anwendung zur Ausführung von GitLab CI/CD Pipelines.

## Setup

Um den Server ans laufen zu bringen sind folgende Schritte notwendig. Hier am Beispiel eines Ubuntu Servers.

1. **Packete installieren**

   ``` shell
   sudo apt install docker.io gitlab-runner
   ```

2. **Docker einrichten**
  
   Damit Docker nicht jedes Image erst noch herunterladen muss macht es Sinn die wichtigsten Images vorzuinstallieren.

   ```shell
   sudo docker pull alpine
   sudo docker pull ubuntu
   sudo docker pull node
   sudo docker pull dockette/vercel
   ```


3. **GitLab Runner einrichten**
   
   ``` shell
   sudo gitlab-runner register
   ```

   Hier werden die Verbindungsdaten von GitLab gefordert. Diese findet man unter<br>
   ``` shell
   Settings > CI / CD > Runners Expand > Set up a specific Runner manually
   ```

   Sind sowohl die `URL` als auch dir `Registration Token` eingetragen, wird man nachfolgend gefragt welches System man für den GitLab Runner aufsetzen möchte.

   Dort ist `docker` einzutragen.

   Als Standardimage kann bspw. `alpine:latest`, oder `ubuntu:latest` verwendet werden.
   
4. **GitLab Runner Konfiguration anpassen**

Um den [#5 - lookup docker on 168.63.129.16:53: no such host](https://gitlab.ei.hs-duesseldorf.de/wetter-station-gruppe-e/luftgut-website/-/issues/5) Fehler zu beheben muss die Konfiguration des GitLab Runners angepasst werden.

```shell
sudo nano /etc/gitlab-runner/config.toml
```

In der Konfigurationsdatei müssen folgende Werte geändert werden:

```toml
[[runners]]
   ...
   [[runner.docker]]
      ...
      privileged = true # war false
      ...
      volumes = ["/cache", "/var/run/docker.sock:/var/run/docker.sock"] # war ["/cache"]
```
   
