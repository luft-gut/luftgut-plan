# Luftgut Wetterstation - Gruppe E
*Günstig, Modular, Open Source.*

⚠️ Work in Progress

## 1. Einleitung
Im Rahmen des Modules "**Embedded Systems 2**" gilt es, für das Wintersemester 2020/21, ein Wetterstation in Gruppenarbeit zu planen und umzusetzen. Für die Entwicklung des Projektes wurde jeder Gruppe ein Forschungsbudget von 500€ bereitgestellt.

  **Mitglieder:**
  <br>Luma Cerquiera Duarte de Araujo - 805118
  <br>Lisa Hannah van Thiel           - 793017
  <br>Cäcilia Frankovna Zouikova      - 792539
  <br>Janina Urner                    - 790679
  <br>Cedric Thysen                   - 786460
  <br>André Kuhlmann                  - 779690

## 2. Produktbeschreibung
Neben den unzähligen Wetterstation die es bereits auf dem Markt gibt soll diese Wetterstation durch Ihre **Benutzerfreundlichkeit** und durch ihr **Preisleistungsverhältnis** punkten. Alle Wetterstationen sollen über eine Internetverbindung ihre Daten mit einer extern gehosteten Datenbank teilen. Ist ein flächendeckendes Netz aus Wetterstationen entstanden könnten die gesammelten Daten zur Berechnung verschiedenster Wettermodelle verwendet werden. Die Hardwareschnittstelle soll Open Source zugäglich gemacht werden, so dass Erweiterungen auch von externen Personen einfach hinzugefügt werden können -> Modularität.

## 4. Einsatzbereich
Die Wetterstation kann entweder im Wohnraum oder draußen platziert werden. Sie soll hauptsächlich in Privathaushalten zum Einsatz kommen und seinem Nutzer eine einfach zugängliche möglichkeit bieten die aktuellen Wetterdaten einzusehen. 

## 5. Anforderungsliste
  1. **Hadeware**<br>
  Der Kern der Wetterstation soll auf einem **Raspberry Pi Zero W** laufen. Dieser Singleboard Computer ist kostengünstig und kommt mit der nötigen WLAN Schnittstelle.

  2. **Sensoren**<br>
    Die Aufgabenstellung sieht es vor, dass alle mit einem ❗️ Ausrufezeichen versehnen Sensoren zum Einsatz kommen.<br>
    
     **Basis-Kit:**
     - ❗️ Temperatur
     - ❗️ Feuchtigkeit
     - ❗️ Luftdruck<br>

     **Outdoor-Kit:**
     - ❗️ Windgeschwindigkeit
     - Windrichtung

  3. **Wetterbeständigkeit**<br>
    Da die Wetterstation sowohl draußen als auch drinnen Verwendet werden kann kommt die Wetterstation in einem Witterungsbstänidgen Gehäuse. Das Gehäuse soll ein an die Wetterstation angepasster 3D Druck werden. Dieser ist kostengünstig und läst sich einfach anpassen.

  4. **Architektur**<br>
  Von der Wetterstation gesammelte Daten werden dezentral auf einem Firebase Server gespeichert und von dort aus an die jeweiligen Endgeräte verteilt. Dies hat zum Vorteil, dass der Nutzer keine umständliche Portfreigabe an seinem Router einrichten muss und ein großer Datensatz gesammelt werden kann.
  ![Server Architektur](https://gitlab.ei.hs-duesseldorf.de/wetter-station-gruppe-e/plan/-/raw/master/assets/architektur.png)

  5. **Positionsbestimmung**<br>
  Um die Position der Wetterstation zu definieren muss der Nutzer entweder zu beginn der Einrichtung die Adresse der Wetterstation angeben oder es wird die IP-Adresse zu lokalisierung verwendet. Da es sich bei Wetterstationen meist um Stationäre Einrichtungen handelt haben wir ein GPS-Modul für unnötigen Aufwand empfunden. 

  6. **Stromversorgung**<br>
  Das Raspberry Pi soll entweder über einen Netzwerkstecker oder über eine kleine Solarzelle mit Strom versorgt werden.

  7. **Dokumentation**<br>
  Damit der Nutzer nicht mit seinem Gerät im Regen stehen gelassen wird ist es wichtig dem Nutzer neben einer intuitiven Bedinung auch eine möglichst ausführliche Beschreibung mit an die Hand zu geben.

## 6. Aufgabenteilung
  - Janina
    - Webseite
    - Materialbestellung
  - Lisa
    - Webseite
  - Luma
    - Mobile Anwendungen - iOS / Android mit Flutter
  - Cissy
    - Firebase 
  - Cedric
    - Firebase
  - André
    -  Hardwareeimplementierung

## 7. Zeitplan
- November 
  - [X] Bestellung aufgeben
  - [ ] Sensor Daten auf Hardware erhalten

- Dezember
  - [ ] Verknüpfen der einzelnen Software Teile
  - [ ] Webseite fertigstellen

Um den Stress vor und in der Prüfungsphase auf ein minimum zu reduzieren sollte das wichtigste bis Neujahr fertig werden.

- Januar
  - [ ] Feinschliff und Fehlerbehebungen

## 8. Produktionskosten

### 8.1 Finanzierung

## 9. Werbung
