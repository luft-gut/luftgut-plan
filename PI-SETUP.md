# Raspberry Pi Development Setup

Die nachfolgenden Anleitungen beschreiben, wie man das Raspberry Pi Zero erfolgreich für die Entwicklung mit NodeJS enrichtet.

### Raspberry Pi auf die SD-Karte flashen

---

1. **Raspberry Pi OS herunterladen**

   Zu beginn muss die neuste Version des Raspberry Pi OS von der offiziellen Raspberry Pi Webseite heruntergeladen werden.

   https://www.raspberrypi.org/downloads/raspberry-pi-os/

   Wir benötigen die **Lite Version**. Diese kommt ohne die Ressourcen Intensive Desktop Umgebung.

2. **Raspberry Pi OS flashen**

   Ist die ZIP Datei heruntergeladen kann über ein weiteres Tool die SD Karte mit dem Betriebssystem beschrieben werden.

   1. Downloaden: https://www.balena.io/etcher/

   2. Ist das Programm Etcher geöffnet kann die eben heruntergeladen ZIP Datei mit dem Raspberry Pi OS ausgewählt werden.
   3. Mit einem klick auf "Select Target" kann das Ziellaufwerk -> unsere SD Karte ausgewählt werden. WICHTIG!!!: Achtet darauf, dass ihr auch wirklich das richtige Ziellaufwerk festgelegt habt. ALLE auf dem ausgewählten Laufwerk befindlichen Daten werden nach klicken auf Flash unwiederruflich gelöscht! Kontrolliert die Laufwerkgröße!
   4. Habt ihr das richtige Laufwerk ausgewählt könnt ihr auf "Flash!" klicken. Dies löscht alle Daten auf der SD Karte und kopiert die nötigen Daten.

3. **Auf das Bootlaufwerk zugreifen**

   1. Nachdem das Flashen der SD-Karte abgeschlossen ist und eure SD-Karte ausgewurfen wurde. Zieht die SD-Karte heraus und steckt sie wieder in euren SD-Kartenleser. 
   2. Es sollte nun ein Laufwerk mit dem Namen boot im Explorer / Finder zu finden sein.

4. **SSH aktivieren**

   Auf dem `boot` Laufwerk müsst ihr nun eine leere Datei mit dem Namen `ssh` erstellen. Die Datei darf keine Dateiendung haben. Diese Datei sagt dem Raspberry Pi beim starten, dass er einen SSH-Server starten soll. 

   🤨 **Was ist ein SSH-Server?**: Ein SSH-Server sorgt dafür, dass man sich von einem anderen PC mit dem Kommandozeile des Raspberry-Pi verbinden kann.

5. **WLAN einrichten**

   Damit wir das Raspberry-Pi auch erreichen können muss eine weitere Datei auf dem `boot` Laufwerk erstellt werden. Diese Datei nen wir nun `wpa_supplicant.conf`.

   In diese Datei kommt folgendes:

   ```
   ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
   update_config=1
   country=DE

   network={
    ssid="<Name des WLAN-Netzwerk>"
    psk="<Password des WLAN-Netzwerk>"
   }
   ```

6. **SD-Karte in den Pi einlegen**

   Die SD-Karte kann nun vom Rechner in den Raspberry Pi eingelegt werden. Und das Pi mit dem Strom verbunden werden.

   Bei dem ersten Start kann es etwas länger dauern bis das Raspberry Pi hochgefahren ist!

### Verbindung mit dem Raspberry Pi herstellen

---

#### macOS

1. **Terminal App öffnen**

2. **SSH Verbindung aufbauen**

   Ist das Raspberry Pi gestartet sollte man sich nun mit dem SSH-Server des Pi verbinden können.

   ```shell
   ssh pi@raspberrypi.local
   ```

   Der Befehl versucht eine SSH Verbindung unter der Adresse `raspberrypi.local` mit den Benutzername `pi` aufzubauen.

3. **Passwort eingeben**

   Das Passwort für den Benutzer `pi` lautet standardgemäß `raspberry`. 



### NodeJS und Johny-Five installieren

---

🧐 **Was ist NodeJS?**: [NodeJS](https://nodejs.org/en/) ist eine Umgebung die es erlaubt JavaScript ausserhalb des Browsers auszuführen.

🤩 **JavaScript**: JavaScript ist eine Programmiersprache die hauptsächlich auf Webseiten aber auch immer mehr auf Backend Servern zum Einsatz kommt.

😮 **Was macht Johnny-Five?**: Dies ist eine Packet das es erlaubt mit der IO des Raspberry-Pis zu interagieren. Es wird über die NPM Packetverwaltung installiert. [Johnny-Five Webseite](http://johnny-five.io)

😤 **What, Paketverwaltung?**: Eine Paketverwaltung hostet von Nutzern erstelle Bibliotheken welche bei NodeJS einfach über den `npm` ([Node Package Manager](https://www.npmjs.com)) Befehl installiert und verwaltet werden können.



1. **Software Updates herunterladen**

   ```shell
   sudo apt update -y && sudo apt upgrade -y && sudo apt autoremove -y
   ```

2. **Pakete installieren**

   ```shell
   sudo apt install -y git i2c-tools libi2c-dev
   ```

3. **I2C Interface aktivieren**<br>
   Damit das Raspberry Pi den Zugriff auf die I2C Schittstelle freigibt muss diese zunächst aktiviert werden.
   ```shell
   sudo raspi-config
   ```

   Befolge nun die folgenden Schritt auf der Oberfläche.<br>
   `Interface Options` > `I2C` > `Yes` > `Finish`

4. **NodeJS installieren** (armv6 Prozessoren - Pi Zero)

   Dies installiert eine bereits kompilierte Version von NodeJS auf dem Raspberry Pi.

   ```shell
   wget https://nodejs.org/dist/latest-v11.x/node-v11.15.0-linux-armv6l.tar.gz
   tar -xzf node-v11.15.0-linux-armv6l.tar.gz
   sudo cp -R node-v11.15.0-linux-armv6l/* /usr/local/
   ```

   Tippt man nun den Befehl `node --version` ein so sollte `v11.15.0` ausgegeben werden. Wenn ja wurde NodeJS erfolgreich installiert.

5. **Johnny-Five installieren**

   Nun da NodeJS eingerichtet ist, können die nötigen Pakete für eine erfolgreiche Verwendung von Johnny-Five installiert werden.

   ```shell
   mkdir projekt
   cd projekt
   npm init -y
   npm i -S johnny-five raspi-io # Wichtig es darf kein sudo verwendet werden
   # Bei der Installation wird man gefragt on man das pigpio Packet installieren möchte. Dies mit einem Klick auf Enter bestätigen.
   ```

6. **Raspberry Pi Neustarten**

   Da `raspi-io` bei der Installation einige Änderungen an der Bootroutine gemacht hat muss das Raspberry-Pi zunächst neu gestartet werden, damit diese Änderungen in Kraft treten.

   ```shell
   sudo reboot
   ```

### Docker

---

Um die Container zu entwickeln, welche hinterher, über Balena, auf die Wetterstationen verteilt werden sollen muss zunächst Docker installiert werden.

⛴ **Was ist Docker?**: [Docker](https://www.docker.com) oder die Docker Engine ist eine Virtualisierungssoftware, also eine Software die es ermöglicht Prozesse in Isolierten *Containern* laufen zu lassen. 
Docker erstellt für jede Anwendung eine Art "Virtualbox", in welcher die Anwendung ungestört laufen kann.<br>
Wenn ihr mehr zu Docker wissen möchtet dann findet ihr hier ein sehr gutes Video [YouTube - Docker in 100 Seconds](https://www.youtube.com/watch?v=Gjnup-PuquQ)

🙇‍ **Wofür brauchen wir Docker?**: Wir verwenden eine Flottenverwaltungssoftware namens [Balena](https://www.balena.io). Eine Aufgabe einer Flottenverwaltungssoftware ist es sämtliche Endgeräte auf dem neusten Stand zu halten. Änderungen an einem Container werden so automatisch an alle Endgeräte verteilt und benötigten, bei bestehender WLAN Verbindung keinerlei Eingreifen des Kunden.

1. **Herunterladen des Installscripts**
   
   ```shell
   curl -fsSL https://get.docker.com -o get-docker.sh
   ```
2. **Ausführen des Installscripts**
   
   ```shell
   sudo sh get-docker.sh
   ```

### Dateibearbeitung

---

Möglichkeiten zur bearbeitung von Dateien auf dem Pi:
- Über das Konsolenprogramm `nano` oder `vim`
- Durch kopieren der Dateien vom PC zum Pi über den `scp` oder `rsync` command
- Durch installieren eines FTP- (Windows / macOS) oder AFP-Servers (macOS) `sudo apt install netatalk`

⚠️ Leider ist es **nicht möglich** das Remote Development Plugin für **VSCode** zu verwenden, welches nicht mit dem ARMv6 Prozessor des Raspberry Pi kompatibel ist.

### Ressourcen

---

[Installationsanleitung Raspberry Pi](https://github.com/nebrius/raspi-io/wiki/Getting-a-Raspberry-Pi-ready-for-NodeBots)

[Johnny-Five Webseite](http://johnny-five.io)


### Pinout

---

Das Pinout für den Pi Zero ist das selbe wie für den "Normalen" Pi.

![Pinout Raspberry Pi - https://www.raspberrypi-spy.co.uk/2012/06/simple-guide-to-the-rpi-gpio-header-and-pins/](https://gitlab.ei.hs-duesseldorf.de/wetter-station-gruppe-e/plan/-/raw/master/assets/pinout.jpg)
<figcaption>https://www.raspberrypi-spy.co.uk/2012/06/simple-guide-to-the-rpi-gpio-header-and-pins/</figcaption>

